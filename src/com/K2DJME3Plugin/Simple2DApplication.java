/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.K2DJME3Plugin;

import com.jme3.app.FlyCamAppState;
import com.jme3.app.SimpleApplication;
import com.jme3.material.Material;
import com.jme3.math.Vector3f;

/**
 * Main class you extend from if you are making a 2D game. same as
 * SimpleApplication but is more for a 2D developer. this comes complete with a
 * 2D physics engine intergrated and a FreeCamera(basicly a 2D FlyCam).
 *
 * @author kinger
 */
public abstract class Simple2DApplication extends SimpleApplication
{

    public static Material BASIC_SPRITE_MATERIAL;
    public static final int CAM_Z_INDEX = 10;

    @Override
    public void simpleInitApp()
    {
        BASIC_SPRITE_MATERIAL = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        initStates();
        simpleInitApp2D();
    }

    private void initStates()
    {
        stateManager.detach(stateManager.getState(FlyCamAppState.class));
        stateManager.attach(new FreeCamera());
    }

    @Override
    public void simpleUpdate(float tpf)
    {
        fixCameraLayer();
        simpleUpdate2D(tpf);
    }

    /**
     * Fix the camera layer.
     */
    private void fixCameraLayer()
    {
        if (cam.getLocation().z
                != CAM_Z_INDEX)
        {
            cam.getLocation().setZ(CAM_Z_INDEX);
            cam.update();
        }
    }

    public void simpleUpdate2D(float tpf)
    {
    }

    public abstract void simpleInitApp2D();
}
