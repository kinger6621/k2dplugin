/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.K2DJME3Plugin;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;

/**
 * This is a 2D free cam.
 *
 * @author kinger
 */
public class FreeCamera extends AbstractAppState implements ActionListener
{

    private Simple2DApplication app;
    private InputManager inputManager;
    private Vector3f cameraTargetPosition = Vector3f.ZERO;
    private Camera cam;
    private static final float speed = 2f;
    private static boolean _goingLeft = false, _goingRight = false, _goingUp = false, _goingDown = false;
    private static final String[] _MAPPINGS =
    {
        "FC_LEFT",
        "FC_RIGHT",
        "FC_UP",
        "FC_DOWN"
    };

    @Override
    public void initialize(AppStateManager stateManager, Application app)
    {
        super.initialize(stateManager, app);
        this.app = (Simple2DApplication) app;

        this.cam = app.getCamera();
        this.inputManager = app.getInputManager();

        initKeys();
    }

    private void initKeys()
    {
        inputManager.addMapping(_MAPPINGS[0], new KeyTrigger(KeyInput.KEY_A));
        inputManager.addMapping(_MAPPINGS[1], new KeyTrigger(KeyInput.KEY_D));
        inputManager.addMapping(_MAPPINGS[2], new KeyTrigger(KeyInput.KEY_W));
        inputManager.addMapping(_MAPPINGS[3], new KeyTrigger(KeyInput.KEY_S));

        inputManager.addListener(this, _MAPPINGS);
    }

    @Override
    public void update(float tpf)
    {
//        Update camera target position
        cameraTargetPosition.x += (_goingLeft ? speed : _goingRight ? -speed : 0)
                * tpf;
        cameraTargetPosition.y += (_goingUp ? -speed : _goingDown ? speed : 0)
                * tpf;
        cam.getLocation().interpolate(cameraTargetPosition, speed * tpf);
        if (cam.getLocation() != cameraTargetPosition)
        {
            cam.update();
        }


    }

    @Override
    public void cleanup()
    {
        super.cleanup();
    }

    public void onAction(String name, boolean isPressed, float tpf)
    {
        if (name.equals(_MAPPINGS[0]))
        {
            _goingLeft = isPressed;
        } else if (name.equals(_MAPPINGS[1]))
        {
            _goingRight = isPressed;
        }
        if (name.equals(_MAPPINGS[2]))
        {
            _goingUp = isPressed;
        } else if (name.equals(_MAPPINGS[3]))
        {
            _goingDown = isPressed;
        }
    }
}
