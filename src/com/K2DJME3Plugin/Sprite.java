/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.K2DJME3Plugin;

import com.jme3.scene.Geometry;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Quad;

/**
 *
 * @author kinger A basic sprite. for now Unshaded material.
 */
public class Sprite extends Geometry
{

//    CONSTRUCTORS
    public Sprite(String name, Quad q)
    {
        super(name, q);
        init();
    }

    public Sprite(String name, float width, float height)
    {
        this(name, new Quad(width, height));
    }

//    Create a simple sprite with a name
    public Sprite(String name)
    {
        this(name, 1f, 1f);
    }

//    Create a simple no name Sprite
    public Sprite()
    {
        this("No name sprite");
    }

//    METHODS
    /**
     * Will init the quad.
     */
    private void init()
    {
//        set up sprite with a basic unshaded material.
        super.setMaterial(Simple2DApplication.BASIC_SPRITE_MATERIAL.clone());
    }
}
