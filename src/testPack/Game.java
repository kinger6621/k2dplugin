/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package testPack;

import com.K2DJME3Plugin.FreeCamera;
import com.K2DJME3Plugin.Simple2DApplication;
import com.K2DJME3Plugin.Sprite;
import com.jme3.material.Material;
import com.jme3.math.Transform;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Quad;
import com.jme3.system.AppSettings;

/**
 *
 * @author kinger
 */
public class Game extends Simple2DApplication
{

    public static void main(String... args)
    {
        Game game = new Game();
        game.setShowSettings(false);
        AppSettings settings = new AppSettings(true);
        settings.setResolution(800, 500);
        game.setSettings(settings);
        game.start();
    }
    //        Some test geometry
    Sprite sprite;

    @Override
    public void simpleInitApp2D()
    {
        rootNode.attachChild(sprite = new Sprite("Simple Sprite"));
    }
    
    @Override
    public void simpleUpdate2D(float tpf)
    {
        sprite.getLocalTranslation().interpolate(Vector3f.UNIT_X.mult(10), tpf);
    }
}
